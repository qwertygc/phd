<?php
session_start();
$want = [
	'couette' 	=> 'Pote de couette',
	'love'		=> 'Du love, du love, du love',
	'friend'	=> 'Juste pote',
	'nothing'	=> 'Je sais pas trop, un peu de tout ?'
];
$orientation = [
	'mec'		=> 'Les mecs',
	'fille'		=> 'les filles',
	'bi'		=> 'Pourquoi choisir ?'
];
$genre = [
	'h'		=> '♂',
	'f'		=> '♀',
	'nb'	=> '⚥'
];
function select($name, $array, $selected_code) {
    $return = '<select name="'.$name.'" id="'.$name.'" class="custom-select">'.PHP_EOL;
    foreach($array as $k => $v) {
		$selected = ($selected_code == $k) ? 'selected' : '';
		$return .= '<option value="'.$k.'" '.$selected.'>'.$v.'</option>'.PHP_EOL;
    }
    $return .= '</select>';
	return $return;
}
if(file_exists('database.db')) {
	$db = new PDO('sqlite:database.db');
}
else {
	$db = new PDO('sqlite:database.db');
	$db->query('CREATE TABLE users (
				id INTEGER PRIMARY KEY,
				pseudo TEXT,
				email TEXT,
				description TEXT,
				genre TEXT,
				orientation TEXT,
				want TEXT,
				token TEXT);
				CREATE TABLE matchs (
				id INTEGER PRIMARY KEY,
				id_matcher INTEGER,
				id_matched INTEGER,
				status INTEGER
				);');
}
function sendemail($sender, $recipient, $subject, $message) {
	$headers = "From:".$sender."\n";
	$headers .= "Content-type: text/html; charset= utf8\n";
	$headers .= "MIME-Version: 1.0\r\n";

	if (mail($recipient, $subject, $message, $headers)) {
		echo "Le message a été envoyé";
	}
	else {
		echo "Le message n'a pas pu être envoyé, veuillez réessayer";
	}
}
?>
<!doctype html>
<html>
	<head>
		<title>🔥 PhDate : le Tinder des labos</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<style>
	* {
		font-family:sans;
		box-sizing:border-box;
	}
	main {
		max-width:100%;
		margin:auto;
	}
	.soloform {
		border: 3px solid #eee;
		padding:2em;
		margin:2em;
		box-sizing: content-box;
		margin:auto;
		max-width:500px;
	}
	a.btn {
		margin: 1em;
	}
	input, label {
		display:block;
		width:100%;
		margin:auto;
	}
	pre {
		background:#eee;
	}
	.card {
		max-width:400px;
		margin:auto;
		border:1px solid transparent;
	}
	.card-link {
		border-radius:5em;
		padding:1em;
		margin:1em;
  		border: 5px solid #EEE;
	}
	footer {
		text-align:center;
	}
	.legend > ul {
	display: grid;
	  height: 100px;
	  grid-template-columns: repeat(6, 1fr);
	  grid-template-rows: 100px;
	}
	.legend  ul{
		list-style-type: none;
	}
	.legend > ul > li ul {
		margin-left:-3em;
	}
	</style>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	</head>
	<body class="container-fluid">
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <a class="navbar-brand" href="index.php">🔥 PhDate : le Tinder des labos</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
		  <li class="nav-item">
		    <a class="nav-link" href="?do=add">Inscription</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="?do=forget">Se connecter</a>
		</li>
		</ul>
	  </div>
	</nav>
<?php
if(isset($_GET['do'])) {
	switch($_GET['do']) {
		case 'add':
			echo '<form method="post" name="addmember" action="?do=add" class="soloform">
					<label for="pseudo">Pseudo Discord : <input type="text" name="pseudo" class="form-control"></label><br>
					<label for="email">Courriel  : <input type="email" name="email" class="form-controlsm"></label><br>
					<label for="description">Description : <textarea name="description" class="form-control"></textarea></label>
					<label for="genre">Genre : '.select('genre', $genre, '').'</label>
					<label for="orientation">Orientation : '.select('orientation', $orientation, '').'</label>
					<label for="want">Recherche : '.select('want', $want, '').'</label>
					<input type="submit"  class="btn btn-primary"/>
				 </form>';
				if(isset($_POST['pseudo'])) {
					$token = sha1(time());
					$query = $db->prepare('INSERT INTO users(pseudo, email, description, genre, orientation, want, token) VALUES(?, ?, ?, ?, ?, ?, ?)');
					$query->execute(array($_POST['pseudo'], $_POST['email'], $_POST['description'], $_POST['genre'], $_POST['orientation'], $_POST['want'], $token));
					$lastid = $db->lastInsertId();
					$url = 'http://qwerty.legtux.org/varia/phder/?do=edit&modify='.$lastid.'&token='.$token;
					echo '<p class="alert alert-success">Bien rajouté ! Voici votre adresse pour modifier votre profil : <a href="'.$url.'">'.$url.'</a>.</p>';
					sendemail('no-reply@legtux.org', $_POST['email'], '[PHDate] Inscription sur PhDate', 'Bonjour, merci de vous êtes inscrit sur PHDate. oici le lien pour modifier votre profil : '.$url.'.');
				}
		break;	
		case 'forget':
			echo '<form method="post" name="forget" action="?do=forget" class="soloform">
					<label for="email">Courriel  : <input type="email" name="courriel" class="form-controlsm"></label><br>
					<input type="submit"  class="btn btn-primary"/>
				 </form>';
			if(isset($_POST['courriel'])) {
				$rq = $db->prepare('SELECT * FROM users WHERE email=?');
				$rq->execute(array($_POST['courriel']));
				$d = $rq->fetch();
				sendemail('no-reply@legtux.org', $_POST['courriel'], '[PHDate] Identifiant oublié', ' Voici le lien pour modifier votre profil : http://qwerty.legtux.org/varia/phder/?do=edit&modify='.$d['id'].'&token='.$d['token']);
			}
		break;
		case 'totoro':
			$rq = $db->prepare('SELECT * FROM users');
			$rq->execute();
			echo '<table class="table">';
			while($d = $rq->fetch()) {
				echo '<tr>
						<td>'.$d['pseudo'].'</td>
						<td>'.$d['email'].'</td>
						<td><input type="text" value="http://qwerty.legtux.org/varia/phder/?do=edit&modify='.$d['id'].'&token='.$d['token'].'"/></td>
					</tr>';
			}
		echo '</table>';
		break;
		case 'edit':
			if(isset($_GET['modify'])) {
					$rq = $db->prepare('SELECT token FROM users WHERE id=?');
					$rq->execute(array($_GET['modify']));
					$token = $rq->fetch();
					if($_GET['token'] == $token['token']) {
						$rq = $db->prepare('SELECT * FROM users WHERE id=?');
						$rq->execute(array($_GET['modify']));
						$d = $rq->fetch();
						echo '<a href="?do=edit&&modify='.$_GET['modify'].'del='.$_GET['modify'].'&token='.$token['token'].'" class="btn btn-danger">Supprimer</a>';
						echo '<a href="?do=directory&user_id='.$d['id'].'&token='.$token['token'].'" class="btn btn-primary" target="_blank">Rechercher des gens</a>';
						echo '<form method="post" name="modify" action="?do=edit&modify='.$_GET['modify'].'&token='.$d['token'].'" class="soloform">
						<label for="pseudo">Pseudo Discord : <input type="text" name="pseudo" value="'.$d['pseudo'].'" class="form-control"></label><br>
						<label for="email">Courriel  : <input type="email" name="email" value="'.$d['email'].'" class="form-controlsm"></label><br>
						<label for="description">Description : <textarea name="description" class="form-control">'.$d['description'].'</textarea></label>
						<label for="genre">Genre : '.select('genre', $genre, $d['genre']).'</label>
						<label for="orientation">Orientation : '.select('orientation', $orientation, $d['orientation']).'</label>
						<label for="want">Recherche : '.select('want', $want, $d['want']).'</label>
						<input type="hidden" name="token" value="'.$d['token'].'"/>
						<input type="hidden" name="id" value="'.$d['id'].'"/>
						<input type="submit"  class="btn btn-primary"/>
					 </form>';
				}
				if(isset($_POST['pseudo']) AND $_POST['token'] == $token['token']) {
					$query = $db->prepare('UPDATE users SET pseudo=?, email=?, description=?, genre=?, orientation=?, want=? WHERE id=?');
					$query->execute(array($_POST['pseudo'], $_POST['email'], $_POST['description'], $_POST['genre'], $_POST['orientation'], $_POST['want'], $_POST['id']));
					echo '<script>location.replace("?do=edit&modify='.$_POST['id'].'&token='.$_POST['token'].'");</script>';
				}

				if(isset($_GET['del'])  AND $_GET['token'] == $token['token']) {
					$rq = $db->prepare('DELETE FROM `contacts` WHERE id=?');
					$rq->execute(array($_GET['del']));
					echo '<script>location.replace("?do=directory");</script>';
				}
			}
		break;
		case 'directory':
			$rq = $db->prepare('SELECT token FROM users WHERE id=?');
			$rq->execute(array($_GET['user_id']));
			$token = $rq->fetch();
			if($_GET['token'] == $token['token']) {
				if(isset($_GET['user_id'])) {
					$rq = $db->prepare('SELECT * FROM users WHERE id=?');
					$rq->execute(array($_GET['user_id']));
					$data = $rq->fetch();
					if($data['want'] == 'friend') {
						$rq = $db->prepare('SELECT * FROM users WHERE want="want"');
						$rq->execute();
					}
					elseif($data['want'] == 'couette' OR $data['want'] == 'love') {
						if($data['orientation'] == 'bi') {
							$orientation = 'genre="h" OR genre="f" OR genre="nb"';
						}
						else {
							if($data['orientation'] == 'fille') {
								$orientation = 'genre="f"  OR genre="nb"';
							}
							else {
								$orientation = 'genre="m"  OR genre="nb"';
							}
						}
						$rq = $db->prepare('SELECT * FROM users WHERE '.$orientation);
						$rq->execute();
					}
					else {
						$rq = $db->prepare('SELECT * FROM users');
						$rq->execute();
					}
					while($d = $rq->fetch()) {
						$matches = $db->prepare('SELECT id_matched FROM matchs WHERE id_matcher=? AND id_matched=?');
						$matches->execute(array($_GET['user_id'], $d['id']));
						$oldmatch = $matches->fetch();
							if(@!in_array($d['id'], $oldmatch)) {
								echo'	<div class="card">
										  <div class="card-body">
											<h5 class="card-title">'.$d['pseudo'].'</h5>
											<h6 class="card-subtitle mb-2 text-muted">'.$genre[$d['genre']].' - '.$want[$d['want']].'</h6>
											<p class="card-text">📖 '.nl2br($d['description']).'</p>
											<a href="?do=directory&matched='.$d['id'].'&user_id='.$_GET['user_id'].'&token='.$_GET['token'].'" class="card-link">💖</a>
											<a href="?do=directory&ignorer='.$d['id'].'&user_id='.$_GET['user_id'].'&token='.$_GET['token'].'" class="card-link">❌</a>';
											
								echo' </div>
										</div>';
							}
						}
				}

			if(isset($_GET['ignorer']) AND isset($_GET['user_id'])) {
				$rq = $db->prepare('INSERT INTO matchs(id_matcher, id_matched, status) VALUES(?,?,?)');
				$rq->execute(array($_GET['user_id'], $_GET['ignorer'], 0));
				echo '<script>location.replace("?do=directory&user_id='.$_GET['user_id'].'&token='.$_GET['token'].'");</script>';
			}
			if(isset($_GET['matched']) AND isset($_GET['user_id'])) {
				$rq = $db->prepare('SELECT pseudo, email FROM users WHERE id=?');
				$rq->execute(array($_GET['matched']));
				$matched = $rq->fetch();
				$rq = $db->prepare('SELECT pseudo, email FROM users WHERE id=?');
				$rq->execute(array($_GET['user_id']));
				$matcheur = $rq->fetch();
				sendemail('no-reply@legtux.org', '[PHDate] '.$matched['email'], 'Un nouveau match', 'Coucou '.$matched['pseudo'].'! Je crois que '.$matcheur['pseudo'].' à un crush pour toi ! Si tu veux parler avec lui, voici son courriel : '.$matcheur['email'].' Sinon hésite pas à lui foutre un vent en prétextant que le courriel est allé direct dans les spams ! À plus dans le bus !');
				echo '<p class="alert alert-success">Match envoyé à  '.$matched['pseudo'].'. Espère juste de pas être tombé directement dans les spams !</p>';
				$rq = $db->prepare('INSERT INTO matchs(id_matcher, id_matched, status) VALUES(?,?,?)');
				$rq->execute(array($_GET['user_id'], $_GET['matched'], 1));
				echo '<script>location.replace("?do=directory&user_id='.$_GET['user_id'].'&token='.$_GET['token'].'");</script>';
			}
		}
		break;
	}
}
?>
</body>
</html>
