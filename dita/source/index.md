---
title: DITA Lab
...

![](logo.png)

# Bienvenue au DITA Lab

Laboratoire de recherche intensive de classe internationale et omni-disciplinaires, regroupant les meilleurs stars-talents de l'excellence mondiale pour disrupter la recherche en innovant agilement. Le laboratoire est une UMR Mixte [Université France Sorbonne](https://twitter.com/realUNIVfrance) et [ANES](http://excellagence.fr/). 

# Nos recherches
- 🍑 PEACH (Pôle Expérimentale d'Analyse CHimique)
- 🍉 PASTEQUE (Pôle Appliquée de Sociologique Territoriale et d'Études QUalitatives Expérimentales)
- 🍒 CHERRY (Centre Historique d'Étude Romaine à la Renaissance Yéménite)
- 🐾 PAW (Pôle d'Astronomie Wormien)
- 🦄 UNICORN (UNIversité COmplémentaire de Rougemontagne Nord)
- *Votre laboratoire ici ?*

# Venir chez nous

Vous souhaitez nous rejoindre ? N'hésitez pas à postuler à notre tenure track en CDD de deux semaines, renouvelable 75 fois ! Nous recrutons sans cesse de nouveaux talents pour enrichir notre pool de recherche !
