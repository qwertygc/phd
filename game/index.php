<?php
error_reporting(-1);
?>
<!doctype html>
	<html>
		<head>
			<title>PhDGame</title>
			<link rel="stylesheet" type="text/css" href="https://framagit.org/qwertygc/Reset.css/-/raw/master/reset.css"/>
			<style>
			body {
	background:transparent;
	color: #424242;
	font-family: Lucida Sans Typewriter, Lucida Console, monaco, Bitstream Vera Sans Mono, monospace;
	font-size:1em;
	line-height:1.5;
	text-rendering:optimizelegibility;
	display:grid;
  grid-template-columns: 33% 33% 33%;
}
a {
	color:#333;
}
.achievements {
	padding: 0.5em;
	border-radius:1em;
	margin: 0.2em;
}
.section1 {
 grid-column: 1;
}
.section2 {
 grid-column: 3;
 max-height:1vh;
}
.section3 {
 grid-column: 2;
}
</style>
		</head>
		<body>
<?php
$achievements = 0;
$totalachievements = 0;
$contentachievements = null;
function addpoints($value) {
	if(!isset($_COOKIE['score'])) {
		setcookie("score", 0);
	}
	else {
		setcookie("score", (int)$_COOKIE['score'] + (int)$value);
	}
}
function readpoints($texte, $cookiename) {
	if(isset($_COOKIE[$cookiename]) AND $_COOKIE[$cookiename] > 0) {
		echo $texte.' : '.$_COOKIE[$cookiename].'<br>';
	}
	else {
		setcookie($cookiename, 0);
	}
}
function resetpoints() {
	foreach($_COOKIE as $cookies=>$values) {
		setcookie($cookies, 0);
	}
}
function propositions($slug, $text, $min, $max, $points, $cookie=false) {
	if(($_COOKIE['score'] >= $min AND $_COOKIE['score'] <= $max)) {
		if($cookie) {
			if($_COOKIE[$cookie['name']] < $cookie['max']) {
				echo '<a href="?points='.$slug.'">'.$text.'</a><br>';
			}
		}
		else {
			echo '<a href="?points='.$slug.'">'.$text.'</a><br>';
		}
	}
	if(isset($_GET['points']) AND $_GET['points'] == $slug) {
		addpoints($points);
		if($cookie) {
			setcookie($cookie['name'], $_COOKIE[$cookie['name']]+$cookie['value']);
		}
		header('Location: index.php');
	}
}
if(isset($_GET['a']) AND $_GET['a'] == 'reset') {
	resetpoints();
	header('Location: index.php');
}
function achievements($palier, $text, $cookie, $color) {
	global $achievements;
	global $totalachievements;
	global $contentachievements;
	$totalachievements +=1;
	if($_COOKIE[$cookie] >= $palier) {
		$contentachievements .= '<div class="achievements" style="background:'.$color.';">'.$text.'</div>';
		$achievements += 1;
	}
}
function malus($palier, $newscore) {
	if($_COOKIE['score'] < $palier) {
		setcookie("score", $newscore);
	}
}
echo '<section class="section1">';
echo '----------<br>';
echo ' <a href="?a=reset">Recommencer</a><br/>';
readpoints('Nombre de points de XP', 'score');
readpoints('Nombre de papiers lus', 'review');
readpoints('Formations ED', 'formationED');
readpoints('Ethique à la recherche', 'ethique');
readpoints('Nombre de podcasts écoutés', 'podcast');
readpoints('Niveau de proscratination', 'proscratination');
readpoints('Nombre de données récoltées', 'data');
readpoints('Nombre de mollets mordus', 'mollets');
readpoints('Nombre de communications', 'acc');
readpoints('Nombre de pages rédigées', 'rediger');
readpoints('Nombre de citations plagiés', 'plagiat');
readpoints('Soutenance', 'soutenance');
readpoints('Nombre de verres bu', 'boire');
readpoints('Patounes', 'paw');
echo '----------<br>';
echo '</section>';


echo '<section class="section3">';
propositions('start', 'Commencer une thèse', 0, 999, 1001); //identifiant - question - nombre de points minimales et max pour afficher la question - nombre de points que ça rapporte
//propositions('startalt', 'Au lieu de faire une thèse, profiter dêtre bac+5 pour être cadre et bien payé', 0, 100, 100000000000);

propositions('readpaper', 'Lire un papier', 1000, 6000, 10, array('name'=>'review', 'value'=>1, 'max'=>1000));
propositions('data', 'Collecter des données (expériences, terrains...)', 4500, 50000, 1000, array('name'=>'data', 'value'=>1, 'max'=>150));
propositions('acc', 'Faire une communication', 10000, 100000000000, 2500, array('name'=>'acc', 'value'=>1, 'max'=>5));
propositions('rediger', 'Rédiger', 50000, 100000000000, (rand(0,20)*1000), array('name'=>'rediger', 'value'=>rand(-5,20), 'max'=>500));
propositions('plagiat', 'Faire du plagiat bien crade !', 50000, 100000000000, -50000, array('name'=>'plagiat', 'value'=>1, 'max'=>10));

propositions('mollets', 'Mordre les mollets du DT 🦝', 10000, 100000000000, rand(-5000, 500), array('name'=>'mollets', 'value'=>1, 'max'=>5));

propositions('formationED', 'Faire une formation lambda de l école doctorale', 1500, 5000, 50, array('name'=>'formationED', 'value'=>1, 'max'=>30));
propositions('ethique', 'Faire la formation éthique de la recherche de l école doctorale', 2000, 100000000000, 1000, array('name'=>'ethique', 'value'=>1, 'max'=>1));

propositions('podcast', 'Proscratiner de manière intelligente en écoutant le podcast bien dans ma thèse ou Grand Labo', 5000, 100000000000, -50, array('name'=>'podcast', 'value'=>25, 'max'=>100));
propositions('glandouiller', 'Proscratiner', 2500, 5000, -250, array('name'=>'proscratination', 'value'=>10, 'max'=>'2500'));

propositions('drink', 'Boire un verre avec des collègues', 3000, 500000000, -100, array('name'=>'boire', 'value'=>1, 'max'=>1000));

propositions('paw', '🐾 Patournées ! 🐾', 1, 100000000000, 0, array('name'=>'paw', 'value'=>rand(1,25), 'max'=>10000000000000000000000000));

if($_COOKIE['rediger'] >= 200) {
	propositions('soutenance', 'Faire la soutenance', 0, 100000000000, 100000000000, array('name'=>'soutenance', 'value'=>1, 'max'=>1));
}
echo '</section>';

echo '<section class="section2">';
achievements(1000, 'Niveau 1 : Bravo, vous avez commencé votre thèse !', 'score', 'Aquamarine');
achievements(5000, 'Niveau 2 : Bravo, vous avez achevé votre première année de thèse !', 'score', 'Aquamarine');
achievements(150, 'Niveau 3 : Bravo, vous avez achevé votre deuxième année de thèse !', 'data', 'Aquamarine');
achievements(200, 'Niveau 4 : Bravo, vous avez achevé votre troisième année de thèse !', 'rediger', 'Aquamarine');
achievements(1, 'Niveau 5 : Bravo, vous êtes docteur !', 'soutenance', 'Aquamarine');

achievements(30, 'Bravo, vous avez fait vos formations de l école doctorale !', 'formationED', 'CornflowerBlue');
achievements(1, 'Bravo, vous avez fait la formation Ethique de la recherche !', 'ethique', 'CornflowerBlue');

achievements(250, 'Bravo, vous avez fini votre revue de littérature !', 'review', 'DarkSalmon');
achievements(150, 'Bravo, vous avez collecté toutes vos données !', 'data', 'DarkSalmon');
achievements(200, 'Bravo, vous avez fini de rédiger !', 'rediger', 'DarkSalmon');

achievements(10, '10 pages, yeah !', 'rediger', 'YellowGreen');
achievements(50, '50 pages, yeah !', 'rediger', 'YellowGreen');
achievements(100, '100 pages, yeah !', 'rediger', 'YellowGreen');
achievements(200, '200 pages, youpiiii !', 'rediger', 'YellowGreen');
achievements(250, '250 pages, ça commence à en faire beaucoup, non ?', 'rediger', 'YellowGreen');
achievements(500, '500 pages, stooooooooooop !', 'rediger', 'YellowGreen');

achievements(1, 'Bravo, vous avez fait votre première communication', 'acc', 'LavenderBlush');
achievements(2, 'Bravo, vous avez fait votre seconde communication', 'acc', 'LavenderBlush');
achievements(3, 'Bravo, vous avez fait votre troisième communication', 'acc', 'LavenderBlush');
achievements(4, 'Bravo, vous avez fait votre quatrième communication', 'acc', 'LavenderBlush');
achievements(5, 'Bravo, vous avez fait votre cinquième communication', 'acc', 'LavenderBlush');

achievements(1, 'Niveau 1 : Un petit verre, ça va', 'boire', 'DeepPink');
achievements(2, 'Niveau 2 : La petite soeur s il vous plait', 'boire', 'DeepPink');
achievements(5, 'Niveau 3 : mzais jz vous ditz ke sa va', 'boire', 'DeepPink');
achievements(10, 'Niveau 4 : hetsturytuiluom sryturyi', 'boire', 'DeepPink');
achievements(15, 'Niveau 5 : Mon ex, revieeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeent je vous aimennnnnnnnnnnnnnnnnnt putaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaainggggggggggggggg', 'boire', 'DeepPink');

achievements(100, 'Niveau 1 : Bravo, vous connaissez par coeur tout les podcasts', 'podcast', 'LemonChiffon');
achievements(10, 'Niveau 2 : Bravo, vous avez regardé Facebook', 'proscratination', 'LemonChiffon');
achievements(100, 'Niveau 3: Bravo, vous avez regardé Twitter', 'proscratination', 'LemonChiffon');
achievements(200, 'Niveau 4 : Bravo, vous avez fini Internet', 'proscratination', 'LemonChiffon');
achievements(250, 'Niveau 5 : Bravo, votre maison brille de mille feux', 'proscratination', 'LemonChiffon');
achievements(500, 'Niveau 6 : Bravo, vous savez faire des licornes en origami', 'proscratination', 'LemonChiffon');
achievements(750, 'Niveau 7 : Est-ce pertinent de regarder cet inième photo d animal mignon ?', 'proscratination', 'LemonChiffon');
achievements(1000, 'Niveau 8 : Docteur en proscratination, bravo !', 'proscratination', 'LemonChiffon');

achievements(1, 'Thèse plagiée, expert en copier-coller', 'plagiat', '#6078F0');

achievements(1, 'Niveau 1 : Patounes normales', 'paw', 'chartreuse');
achievements(10, 'Niveau 2 : Patounes blanches', 'paw', 'chartreuse');
achievements(25, 'Niveau 3 : Patounes jaunes', 'paw', 'chartreuse');
achievements(50, 'Niveau 4 : Patounes oranges', 'paw', 'chartreuse');
achievements(100, 'Niveau 5 : Patounes vertes', 'paw', 'chartreuse');
achievements(250, 'Niveau 6 : Patounes bleues', 'paw', 'chartreuse');
achievements(500, 'Niveau 7 : Patounes rouges', 'paw', 'chartreuse');
achievements(1000, 'Niveau 8 : Patounes violettes', 'paw', 'chartreuse');
achievements(2500, 'Niveau 9 : Patounes noires', 'paw', 'chartreuse');
achievements(2500, 'Niveau 10 : Patounes rainbows', 'paw', 'chartreuse');

achievements(1, 'Raton mordeur de mollets', 'mollets', 'SeaGreen');
achievements(5, 'Raton sans dents à force de manger des mollets', 'mollets', 'SeaGreen');

//malus(1000, 0);

echo '🏆 '.$achievements.'/'.$totalachievements;
echo $contentachievements;
echo '</section>';
?>
</body>
</html>
