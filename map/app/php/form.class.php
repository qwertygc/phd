<?php
class form {
	
	function __construct($array) {
	# $array (array) array of attribut 'attribut'=>'value' give attribut='value'
		$form ='';
		foreach($array as $key => $value) {
			$form .= $key.'="'.$value.'" ';
		}
		echo '<form '.$form.'>'.PHP_EOL;
	}
	function input($array) {
	# $array (array) array of attribut 'attribut'=>'value' give attribut='value'
		$form ='';
		foreach($array as $key => $value) {
			$form .= $key.'="'.$value.'" ';
		}
		echo '<input '.$form.'/>'.PHP_EOL;
	}
	function checkbox($id, $label, $array, $checked) {
	# $id (string) Your id's checkbox
	# $label (string) Text's label
	# $array (array) array of attribut 'attribut'=>'value' give attribut='value'
	# $checked (bool) : if it's checked or not
		$form ='';
		foreach($array as $key => $value) {
			$form .= $key.'="'.$value.' ';
		}
		if($checked) {
			$form .= 'checked = "checked"';
		}
		echo '<label for="'.$id.'"><input '.$form.' id="'.$id.'" name="'.$id.'" type="checkbox">'.$label.'</label>'.PHP_EOL;
	}
	function label($name, $entry) {
		#$name : name=""(string)
		#$entry : string)
		echo '<label for="'.$name.'">'.$entry.'</label>'.PHP_EOL;
	}
	function select($arg, $array, $select='', $keyvalue=false) {
		#$name : name=""(string)
		#$array : value posted in select 'value'=>'view' (Array)
		#$keyvalue : if true : 'value'=>'view' else 'value' (Boolean)
		$selected = '';
		$select_arg ='';
		foreach($arg as $key => $value) {
			$select_arg .= $key.'="'.$value.'" ';
		}
		$form= '<select '.$select_arg.'">'."\n";
		if($keyvalue) {
			foreach($array as $key => $option) {
				if($select === $key) {
					$selected = ' selected="selected"';
				}
				$form .= "\t".'<option value="'.$key.'"'.$selected.'>'.$option.'</option>'."\n";
				$selected='';
			}
		}
		else {
			foreach($array as $option) {
				if($select === $option) {
					$selected = ' selected="selected"';
				}
				$form .= "\t".'<option value="'.$option.'"'.$selected.'>'.$option.'</option>'."\n";
				$selected='';
			}
		}
		$form .= '</select>'.PHP_EOL;
		echo $form;
	}
	function textarea($array, $content) {
		# $array (array) array of attribut 'attribut'=>'value' give attribut='value'
		# $content (string) is content of textarea
		$form ='';
		foreach($array as $key => $value) {
			$form .= $key.'="'.$value.'" ';
		}
		echo '<textarea '.$form.'>'.$content.'</textarea>'.PHP_EOL;
	}
	function startfieldset($legend) {
		echo '<fieldset>'.PHP_EOL;
		echo '<legend>'.$legend.'</legend>'.PHP_EOL;
	}
	function endfieldset() {
		echo '</fieldset>'.PHP_EOL;
	}
	function endform() {
		echo "</form>".PHP_EOL;
	}
	function __destruct() {
    }
	
}
