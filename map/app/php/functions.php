<?php
function select_field($academic_field, $selected_code) {
	ksort($academic_field);
    $gpd_field = array();
    foreach($academic_field as $k => $field) {
        $gpd_field[$field['cat']][$k] = $field['field'];
    }
    echo '<select name="field" id="field" class="custom-select">'.PHP_EOL;
    foreach($gpd_field as $k => $v) {
        echo '<optgroup label="'.$k.'">'.PHP_EOL;
        foreach($v as $code => $field) {
            $selected = ($selected_code == $code) ? 'selected' : '';
            echo '<option value="'.$code.'" '.$selected.'>'.$field.'</option>'.PHP_EOL;
        }
        echo '</optgroup>'.PHP_EOL;
    }
    echo '</select>';
}
function legend($academic_field) {
	ksort($academic_field);
    $gpd_field = array();
    foreach($academic_field as $k => $field) {
        $gpd_field[$field['cat']][$k] = array('field'=>$field['field'], 'colour'=>$field['colour']);
    }
	$return = '<ul>';
	foreach($gpd_field as $k => $v) {
		$return .= '<li>'.$k.'<ul>';
		foreach($v as $code => $field) {
			$colour = str_replace('Icon', '', $field['colour']);
			$return .=  '<li><img src="app/img/marker-icon-2x-'.$colour.'.png" style="height:25px;"/> '.$field['field'].'</li>';
		}
		$return .=  '</ul></li>';
	}
	$return .=  '</ul>';
	return $return;
}
$academic_field = array (
    'agro'          =>  array('colour'=> 'blueIcon', 'field' =>'Agronomie', 'cat'=>'Sciences appliquées'),
    'biomed'        =>  array('colour'=> 'blueIcon', 'field' =>'Biomédical', 'cat'=>'Sciences appliquées'),
    'genie'         =>  array('colour'=> 'blueIcon', 'field' =>'Génie industriel/civil', 'cat'=>'Sciences appliquées'),
    'mat'           =>  array('colour'=> 'blueIcon', 'field' =>'Matériaux', 'cat'=>'Sciences appliquées'),
    'scinge'           =>  array('colour'=> 'blueIcon', 'field' =>'Sciences de l’ingénieur', 'cat'=>'Sciences appliquées'),
    'sante'         =>  array('colour'=> 'blueIcon', 'field' =>'Santé', 'cat'=>'Sciences appliquées'),
   'staps'         =>  array('colour'=> 'blueIcon', 'field' =>'STAPS', 'cat'=>'Sciences appliquées'),
   'gelec'         =>  array('colour'=> 'blueIcon', 'field' =>'Génie électrique', 'cat'=>'Sciences appliquées'),


    'archeo'           =>  array('colour'=> 'redIcon', 'field' =>'Archéologie', 'cat'=>'SHS'),
    'art'           =>  array('colour'=> 'redIcon', 'field' =>'Art et culture', 'cat'=>'SHS'),
    'crimina'       =>  array('colour'=> 'redIcon', 'field' =>'Criminalistique', 'cat'=>'SHS'),
    'design'        =>  array('colour'=> 'redIcon', 'field' =>'Design', 'cat'=>'SHS'),
    'droit'         =>  array('colour'=> 'redIcon', 'field' =>'Droit', 'cat'=>'SHS'),
    'econ'          =>  array('colour'=> 'redIcon', 'field' =>'Économie', 'cat'=>'SHS'),
    'geog'          =>  array('colour'=> 'redIcon', 'field' => 'Géographie', 'cat'=>'SHS'),
    'hist'          =>  array('colour'=> 'redIcon', 'field' =>'Histoire', 'cat'=>'SHS'),
    'infocom'       =>  array('colour'=> 'redIcon', 'field' =>'Info-Com', 'cat'=>'SHS'),
    'ling'          =>  array('colour'=> 'redIcon', 'field' => 'Linguistique', 'cat'=>'SHS'),
   'pol'           =>  array('colour'=> 'redIcon', 'field' =>'Politiques et relations internationales', 'cat'=>'SHS'),
    'psy'           =>  array('colour'=> 'redIcon', 'field' =>'Psychologie', 'cat'=>'SHS'),
   	'edu'           =>  array('colour'=> 'redIcon', 'field' =>'Sciences de l’éducation', 'cat'=>'SHS'),
    'gestion'       =>  array('colour'=> 'redIcon', 'field' =>'Sciences de gestion', 'cat'=>'SHS'),
    'socio'         =>  array('colour'=> 'redIcon', 'field' =>'Sociologie', 'cat'=>'SHS'),

    'atmo'           =>  array('colour'=> 'greenIcon', 'field' =>'Atmosphère', 'cat'=>'Sciences naturelles'),
    'bio'           =>  array('colour'=> 'greenIcon', 'field' =>'Biologie', 'cat'=>'Sciences naturelles'),
    'chimie'        =>  array('colour'=> 'greenIcon', 'field' =>'Chimie', 'cat'=>'Sciences naturelles'),
    'ecol'          =>  array('colour'=> 'greenIcon', 'field' =>'Écologie', 'cat'=>'Sciences naturelles'),
    'neuro'         =>  array('colour'=> 'greenIcon', 'field' =>'Neurosciences', 'cat'=>'Sciences naturelles'),
    'phy'           =>  array('colour'=> 'greenIcon', 'field' =>'Physique', 'cat'=>'Sciences naturelles'),
 	'svt'           =>  array('colour'=> 'greenIcon', 'field' =>'Sciences de la Terre', 'cat'=>'Sciences naturelles'),

    'info'          =>  array('colour'=> 'orangeIcon', 'field' =>'Informatique', 'cat'=>'Informatique et Mathématiques'),
    'mathapp'       =>  array('colour'=> 'orangeIcon', 'field' =>'Mathématiques appliquées', 'cat'=>'Informatique et Mathématiques'),
    'rien'          =>  array('colour'=> 'violetIcon', 'field' =>'Rien de tout ça', 'cat'=>'Rien de tout ça'),
);
