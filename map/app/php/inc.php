<?php
error_reporting(-1);
if(file_exists('database.db')) {
	$db = new PDO('sqlite:database.db');
}
else {
	$db = new PDO('sqlite:database.db');
	$db->query('CREATE TABLE contacts (
				id INTEGER PRIMARY KEY,
				pseudo TEXT,
				thesis TEXT,
				url TEXT,
				skills TEXT,
                field TEXT,
				email TEXT,
				favori INTEGER,
				token TEXT,
				loc TEXT,
				lat TEXT,
				lon TEXT
			);');
}
include 'functions.php';
include 'geo.functions.php';
include 'misc.functions.php';
include 'form.class.php';
include 'table.class.php';
