<?php
function redirect($url) {
	@header('Location: '.$url);
	echo '<meta http-equiv="refresh" content="0; url='.$url.'">';
	die;
}
function send_mail($to, $from='no-reply@example.org', $subject = '(No subject)', $message = '') {
	$nl = (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $to)) ? $nl = "\r\n" : "\n";
	$message_html = "<html><head></head><body>".$message."</body></html>";
	$message_txt = strip_tags($message);
	$boundary = "-----=".md5(rand());
	$header = "From: ".$from."".$nl;
	$header.="X-Mailer: PHP/".phpversion();
	$header.= "MIME-Version: 1.0".$nl;
	$header.= "Content-Type: multipart/alternative;".$nl." boundary=\"$boundary\"".$nl;
	$body = $nl."--".$boundary.$nl;
	$body.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$nl;
	$body.= "Content-Transfer-Encoding: 8bit".$nl;
	$body.= $nl.$message_txt.$nl;
	$body.= $nl."--".$boundary.$nl;
	$body.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$nl;
	$body.= "Content-Transfer-Encoding: 8bit".$nl;
	$body.= $nl.$message_html.$nl;
	$body.= $nl."--".$boundary."--".$nl;
	$body.= $nl."--".$boundary."--".$nl;
	mail($to,'=?UTF-8?B?'.base64_encode($subject).'?=',$body,$header);
}
function generateRandomString($length = 10) {
    $characters = '234567ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
function getRequestProtocol() {
    if(!empty($_SERVER['HTTP_X_FORWARDED_PROTO']))
        return $_SERVER['HTTP_X_FORWARDED_PROTO'];
    else 
        return !empty($_SERVER['HTTPS']) ? "https" : "http";
}
