<?php
class table {
	
	function __construct($metadata) {
	# $array (array) array of attribut 'attribut'=>'value' give attribut='value'
		$table ='';
		foreach($metadata as $key => $value) {
			$table .= $key.'="'.$value.'" ';
		}
		echo '<table '.$table.'>'.PHP_EOL;
	}
	function head($array, $metadata=array()) {
	# $array (array) array of attribut 'attribut'=>'value' give attribut='value'
		$table ='';
		foreach($metadata as $key => $value) {
			$table .= $key.'="'.$value.'" ';
		}
		echo '<tr>'.PHP_EOL;
		foreach($array as $value) {
			echo '<th>'.$value.'</th>'.PHP_EOL;
		}
		echo '</tr>'.PHP_EOL;
	}
	function row($array, $metadata=array()) {
	# $array (array) array of attribut 'attribut'=>'value' give attribut='value'
		$table ='';
		foreach($metadata as $key => $value) {
			$table .= $key.'="'.$value.'" ';
		}
		echo '<tr '.$table.'>'.PHP_EOL;
		foreach($array as $value) {
			echo '<td>'.$value.'</td>';
		}
		echo '</tr>'.PHP_EOL;
	}

	function endtable() {
		echo "</table>".PHP_EOL;
	}
	function __destruct() {
    }
	
}
