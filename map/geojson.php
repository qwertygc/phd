<?php
header('content-type:application/json');
include 'app/php/inc.php';

$geojson = array();
$geojson['type'] = 'FeatureCollection';
$rq = $db->query('SELECT * FROM contacts ORDER BY loc ASC');
while($row = $rq->fetch()) {
	$row['lon'] = isset($row['lon']) ? $row['lon'] : '-68.70873175';
	$row['lat'] = isset($row['lat']) ? $row['lat'] : '33.012650657135424';
	
	$geojson['features'][] = array (
		'type' => 'Feature',
		'properties' => array (
			'name' => '<b>'.htmlentities($row['pseudo']).'</b> - <i>'.$academic_field[$row['field']]['field'].'</i> - '.htmlentities($row['thesis']),
		),
		'geometry' => array (
			'type' => 'Point',
			'coordinates' => array((float)$row['lat'], (float)$row['lon'])
		)
	);
}

echo json_encode($geojson);
