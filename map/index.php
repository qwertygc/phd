<?php
include 'app/php/inc.php';
?>

<!doctype html>
<html>
	<head>
		<title>Carte de localisation des membres</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="app/css/style.css" />
		<link rel="stylesheet" href="app/css/leaflet.css" />
		<script src="app/js/leaflet.js"></script>
		<script src='app/js/leaflet.markercluster.js'></script>
		<script src='app/js/leaflet-color-markers.js'></script>
		<link rel="stylesheet" type="text/css" href="app/css/MarkerCluster.css" />
		<link rel="stylesheet" type="text/css" href="app/css/MarkerCluster.Default.css" />
		<link rel="stylesheet" href="app/css/bootstrap.min.css" />
		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="app/js/bootstrap.min.js"></script>		
		<script src="app/js/sorttable.js"></script>
	</head>
	<body class="container-fluid">
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <a class="navbar-brand" href="index.php">Carte des doctorant⋅es</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
        <?php
            $rq = $db->query('SELECT COUNT(*) as count FROM contacts');
            $data = $rq->fetch();
            $alldoctorants = $data['count'];
 			$rq = $db->query('SELECT * FROM contacts ORDER BY id DESC LIMIT 1');
            $hello = $rq->fetch();
        ?>
          <li class="nav-item">
            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true"><?php echo $alldoctorants; ?> doctorant⋅es</a>
          </li>
		  <li class="nav-item">
		    <a class="nav-link" href="?do=add">Ajouter un⋅e doctorant⋅e</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" href="?do=connexion">Modifier ses données</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" href="?do=directory">Annuaire</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" href="?do=stats">Statistiques</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" href="?do=about">À propos</a>
		  </li>
		</ul>
		<form class="form-inline my-2 my-lg-0">
			<input class="form-control mr-sm-2" type="search" placeholder="Rechercher" name="q" aria-label="Rechercher">
			<input type="hidden" value="directory" name="do"/>
		  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher</button>
		</form>
	  </div>
	</nav>
<?php
if(isset($_GET['do'])) {
	switch($_GET['do']) {
		case 'cron':
			if(!is_dir('backup')) {
				mkdir('backup');
			}
			else {
				$newfile = 'database_'.date('YmdHis', time()).'db';
				copy('database.db', 'backup/'.$newfile);
				echo 'Sauvegarde de backup/'.$newfile; 
			}
		break;
        case 'about':
            echo '<p>Cartographie des doctorant⋅es du <a href="https://discord.gg/pVptqqP">Discord PHD STUDENTS</a>. Sur une idée de MélanieM et une réalisation de Qwerty (<a href="https://framagit.org/qwertygc/phd/map">Code source</a>, n’hésitez pas à contribuer !). </p>';
            echo '<p>Crédits :
                    <ul>
                        <li><a href="https://leafletjs.com/">Leaflet</a></li>
                        <li><a href="https://github.com/pointhi/leaflet-color-markers">Leaflet Color Markers</a></li>
                        <li><a href="https://github.com/Leaflet/Leaflet.markercluster">Leatflet Marker Cluster</a></li>
                        <li><a href="https://nominatim.openstreetmap.org">Nominatim</a></li>
                        <li><a href="https://getbootstrap.com/">Bootstrap</a></li>
						<li><a href="https://d3js.org/">D3.js</a></li>
						<li><a href="http://bl.ocks.org/JacquesJahnichen/42afd0cde7cbf72ecb81">Jacques Jahnichen</a></li>
                    </ul></p>';
        break;
		case 'dev':
		$query = $db->query('SELECT * FROM contacts');
		$table = New table(array('class'=>'table'));
		$table->head(array('Modifier', 'Supprimer', 'URL', 'ID', 'Pseudo', 'these', 'champ', 'page perso', 'loc', 'lat', 'lon', 'token', 'email'));
		while($d = $query->fetch()) {
			$table->row(array(
				'<a href="?do=edit&modify='.$d['id'].'&token='.$d['token'].'" class="btn btn-success">Modifier</a>',
				'<a href="http://qwerty.legtux.org/varia/PhDMap/?do=edit&modify='.$d['id'].'&del='.$d['id'].'&token='.$d['token'].'" class="btn btn-danger">Supprimer</a>',
				'<input type="text" style="width: 100px;" value="http://qwerty.legtux.org/varia/PhDMap/?do=edit&modify='.$d['id'].'&token='.$d['token'].'"/>',
				$d['id'],
				($d['favori'] == 1 ? '★ ' : '').$d['pseudo'],
				$d['thesis'],
				$d['field'],
				$d['url'],
				$d['loc'],
				$d['lat'],
				$d['lon'],
				$d['token'],
				$d['email']
			));
		}
		$table->endtable();
		break;
		case 'add':
			$form = New form(array('method'=>'post', 'name'=>'addmember', 'action'=>'?do=add', 'class'=>'soloform'));
			$form->label('pseudo', 'Pseudo Discord');
			$form->input(array('type'=>'text', 'name'=>'pseudo', 'class'=>'form-control', 'aria-describedby'=>'pseudo'));
			echo '<small id="pseudo" class="form-text text-muted">Quel est votre pseudo discord ?</small>';
			$form->label('thesis', 'Sujet de thèse');
			$form->input(array('type'=>'text', 'name'=>'thesis', 'class'=>'form-control', 'aria-describedby'=>'thesis'));
			echo '<small id="thesis" class="form-text text-muted">Le sujet de votre thèse. Pas forcément le titre officiel, ça peut être un descriptif un peu plus « vulgarisé » si vous le souhaitez.</small>';
			$form->label('field', 'Champ disciplinaire');
            select_field($academic_field, 'rien');
            
			$form->label('loc', 'Localisation');
			$form->input(array('type'=>'text', 'name'=>'loc', 'class'=>'form-control', 'aria-describedby'=>'loc'));
			echo '<small id="loc" class="form-text text-muted">La ville où vous faites votre thèse. Pas la peine de rentrer une adresse complète, le nom de la ville suffit.</small>';
			$form->label('url', 'Page personnelle');
			$form->input(array('type'=>'url', 'name'=>'url', 'class'=>'form-control'));
			$form->label('email', 'Courriel');
			$form->input(array('type'=>'email', 'name'=>'email', 'id'=>'email', 'class'=>'form-control'));
			$form->label('skills', 'Compétences');
			$form->input(array('type'=>'text', 'name'=>'skills', 'id'=>'skills', 'class'=>'form-control', 'aria-describedby'=>'skills'));
			echo '<small id="skills" class="form-text text-muted">Vous avez des compétences particulières que vous souhaitez faire profiter la communauté ?</small>';
			$form->input(array('type'=>'submit', 'class'=>'btn btn-primary'));
			$form->endform();
			if(isset($_POST['pseudo'])) {
				$geocodage = geocodage($_POST['loc']);
				$lat = $geocodage['lat'];
				$lon =$geocodage['lon'];
				$token = sha1(time());
				$query = $db->prepare('INSERT INTO contacts(pseudo, thesis, field, url, email, skills, loc, lat, lon, token) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
				$query->execute(array($_POST['pseudo'], $_POST['thesis'], $_POST['field'], $_POST['url'], $_POST['email'], $_POST['skills'], $_POST['loc'], $lat, $lon, $token));
				$lastid = $db->lastInsertId();
				$url = 'http://qwerty.legtux.org/varia/PhDMap/?do=edit&modify='.$lastid.'&token='.$token;
				echo '<p class="alert alert-success">Bien rajouté ! Voici votre adresse pour modifier votre profil : <a href="'.$url.'">'.$url.'</a>.</p>';
				send_mail($_POST['email'], 'no-reply@legtux.org',  'Inscription sur la carte des doctorant⋅es', 'Bonjour, merci de vous êtes inscrit sur la carte des doctorant⋅es. Voici le lien pour modifier votre profil : '.$url.'');
			}
		break;
		case 'connexion':
			$form = New form(array('method'=>'post', 'name'=>'connexion', 'action'=>'?do=connexion', 'class'=>'soloform'));
			$form->label('email', 'Courriel');
			$form->input(array('type'=>'email', 'name'=>'email', 'id'=>'email', 'class'=>'form-control', 'aria-describedby'=>'connexionemail'));
			echo '<small id="connexionemail" class="form-text text-muted">Votre courriel avec lequelle vous êtes inscrit. Un lien de connexion vous sera envoyé.</small>';
			$form->input(array('type'=>'submit', 'class'=>'btn btn-primary'));
			$form->endform();
			if(isset($_POST['email'])) {
				$rq = $db->prepare('SELECT * FROM contacts WHERE email=?');
				$rq->execute(array($_POST['email']));
				$d = $rq->fetch();
				$url = 'http://qwerty.legtux.org/varia/PhDMap/?do=edit&modify='.$d['id'].'&token='.$d['token'];
				echo '<p class="alert alert-success">Salut '.$d['pseudo'].' ! Un courriel vous a été envoyé avec un lien de connexion !</p>';
				send_mail($d['email'], 'no-reply@legtux.org', 'Se connecter sur la carte des doctorant⋅es', ' Voici le lien pour modifier votre profil : '.$url);
				header('Location: '.$url);
			}
		break;
		case 'directory':
			if(isset($_GET['q'])) {
				$rq = $db->prepare("SELECT * FROM contacts WHERE pseudo LIKE ? OR thesis LIKE ? ORDER BY pseudo");
				$rq->execute(array("%$_GET[q]%", "%$_GET[q]%"));
			}
			elseif(isset($_GET['field'])) {
				$rq = $db->prepare("SELECT * FROM contacts WHERE field LIKE ? ORDER BY pseudo");
				$rq->execute(array("%$_GET[field]%"));
			}
			elseif(isset($_GET['id'])) {
				$rq = $db->prepare("SELECT * FROM contacts ORDER BY id");
				$rq->execute(array());
			}
			else {
				$rq = $db->query('SELECT * FROM contacts ORDER BY lower(pseudo) ASC');
			}
			$table = New table(array('class'=>'table'));
			$table->head(array('Pseudo', 'Sujet de thèse', 'Discipline', 'Ville', 'Compétences'), array('class'=>'sorttable_no_sort'));
			while($row = $rq->fetch()) {
				$table->row(array(
						'<a href="?do=user&id='.$row['id'].'">'.$row['pseudo'].'</a>',
						$row['thesis'],
						'<a href="?do=directory&field='.$row['field'].'">'.$academic_field[$row['field']]['field'].'</a>',
						$row['loc'],
						$row['skills']));
			}
			$table->endtable();
		break;
		case 'stats':
			$allwords = null;
			$rq = $db->query('SELECT thesis FROM contacts ORDER BY id ASC');
			while($row = $rq->fetch()) {
				$allwords .= $row['thesis'].' ';
			}
		    $req = $db->query('SELECT field, COUNT(*) as stat FROM contacts GROUP BY field');
            $tree = array();
			$stats = array();
            $tree['name'] = 'Doctorants';
            while($data = $req->fetch()) {
				$stats[$data['field']] = (isset($data['stat'])) ? $data['stat'] : 0;
		    }
			foreach(array_unique(array_column($academic_field, 'cat')) as $k => $v) {
				foreach($academic_field as $c => $field) {
					if($v == $field['cat']) {
						$child[]  = array('name'=>$field['field'], 'value'=>@$stats[$c]);
					}
				}
				$tree['children'][] = array('name'=>$v, 'children' => $child);
				unset($child);
			}
			file_put_contents('stats.json', json_encode($tree,  JSON_PRETTY_PRINT));
			echo '<iframe src="app/js/stats.html" style="width:100%; height:90vh; border:0;"></iframe>';
		break;
		case 'user':
			if(isset($_GET['id'])) {
				$rq = $db->prepare('SELECT * FROM contacts WHERE id=?');
				$rq->execute(array($_GET['id']));
				$d = $rq->fetch();
				//echo '<a href="?do=edit&modify='.$_GET['id'].'&token='.$d['token'].'" class="btn btn-success">Modifier</a>';
				echo'	<div class="card">
						  <div class="card-body">
							<h5 class="card-title">'.$d['pseudo'].'</h5>
							<h6 class="card-subtitle mb-2 text-muted">🎓 '.$academic_field[$d['field']]['field'].'</h6>
							<p class="card-text">📖 '.$d['thesis'].'</p>
							<p class="card-text">🧰 '.$d['skills'].'</p>
							<a href="https://www.openstreetmap.org/?mlat='.$d['lon'].'&mlon='.$d['lat'].'" class="card-link">📍 '.$d['loc'].'</a>';
				echo ($d['url'] != '') ? '<a href="'.$d['url'].'" class="card-link">🔗 Page personnelle</a>' : '';
				echo' </div>
						</div>';
			}
		break;
		case 'edit':
			if(isset($_GET['modify'])) {
					$rq = $db->prepare('SELECT token FROM contacts WHERE id=?');
					$rq->execute(array($_GET['modify']));
					$token = $rq->fetch();
					if($_GET['token'] == $token['token']) {
						$rq = $db->prepare('SELECT * FROM contacts WHERE id=?');
						$rq->execute(array($_GET['modify']));
						$d = $rq->fetch();
						echo '<a href="?do=edit&&modify='.$_GET['modify'].'del='.$_GET['modify'].'&token='.$token['token'].'" class="btn btn-danger">Supprimer</a>';
						$form = New form(array('method'=>'post', 'name'=>'addmember', 'action'=>'?do=edit&modify='.$_GET['modify'].'&token='.$token['token'].'', 'class'=>'soloform'));
						$form->label('pseudo', 'Pseudo Discord');
						$form->input(array('type'=>'text', 'name'=>'pseudo', 'class'=>'form-control', 'value'=>$d['pseudo']));
						$form->label('thesis', 'Sujet de thèse');
						$form->input(array('type'=>'text', 'name'=>'thesis', 'class'=>'form-control', 'value'=>$d['thesis']));
						$form->label('field', 'Champ disciplinaire');
						select_field($academic_field, $d['field']);
						$form->label('loc', 'Localisation');
						$form->input(array('type'=>'text', 'name'=>'loc', 'class'=>'form-control', 'value'=>$d['loc']));
						$form->label('url', 'Page personnelle');
						$form->input(array('type'=>'url', 'name'=>'url', 'class'=>'form-control', 'value'=>$d['url']));
						$form->label('email', 'Courriel');
						$form->input(array('type'=>'email', 'name'=>'email', 'class'=>'form-control', 'value'=>$d['email']));
						$form->label('skills', 'Compétences');
						$form->input(array('type'=>'text', 'name'=>'skills', 'id'=>'skills', 'class'=>'form-control'));
						$form->input(array('type'=>'hidden', 'name'=>'token', 'value'=>$d['token']));
						$form->input(array('type'=>'hidden', 'name'=>'id', 'value'=>$d['id']));
						$form->input(array('type'=>'submit', 'class'=>'btn btn-primary'));
						$form->endform();
				}
				if(isset($_POST['pseudo']) AND $_POST['token'] == $token['token']) {
					$rq = $db->prepare('UPDATE contacts SET pseudo=?, thesis=?, field=?, url=?, email=?, skills=?, loc=?, lat=?, lon=? WHERE id=?');
					$reloc = geocodage($_POST['loc']);
					$rq->execute(array($_POST['pseudo'], $_POST['thesis'], $_POST['field'], $_POST['url'], $_POST['email'], $_POST['skills'], $_POST['loc'], $reloc['lat'], $reloc['lon'], $_POST['id']));
					redirect('?do=user&id='.$_POST['id']);
				}
				if(isset($_GET['del'])  AND $_GET['token'] == $token['token']) {
					$rq = $db->prepare('DELETE FROM `contacts` WHERE id=?');
					$rq->execute(array($_GET['del']));
					redirect('?do=directory');
				}
			}
		break;
			
	}
}
else {
	$marker = null;
	$clusters = null;
	$rq = $db->query('SELECT * FROM contacts ORDER BY loc ASC');
	while($row = $rq->fetch()) {
		$row['lon'] = isset($row['lon']) ? $row['lon'] : '-68.70873175';
		$row['lat'] = isset($row['lat']) ? $row['lat'] : '33.012650657135424';
		$nocluster = (empty($_GET['nocluster'])) ? '' : '.addTo(carte)';
		$marker .= 'var marker'.$row['id'].' = L.marker(['.$row['lon'].', '.$row['lat'].'], {icon: '.$academic_field[$row['field']]['colour'].'})'.$nocluster.';
					 marker'.$row['id'].'.bindPopup("<b>'.htmlentities($row['pseudo']).'</b> - <i>'.$academic_field[$row['field']]['field'].'</i> - '.htmlentities($row['thesis']).'");
					var mapopup'.$row['id'].' = marker'.$row['id'].'.getPopup();'.PHP_EOL;
		$clusters .= 'markers.addLayer(marker'.$row['id'].');';
	}
	echo '<div class="alert alert-info" role="alert">Bienvenue à <a href="?do=user&id='.$hello['id'].'">'.$hello['pseudo'].'</a></div>';
	echo '<div id="macarte" style="width:100%; height:85vh"></div>';
	echo '<div class="legend">'.legend($academic_field).'</div>';	
}

if(!isset($_GET['do'])) {
   echo ' <script>
    var carte = L.map(\'macarte\').setView([46.363, 2.984], 4);
    L.tileLayer(\'https://{s}.tile.osm.org/{z}/{x}/{y}.png\', {
        attribution: \'&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors\'
    }).addTo(carte);';
	echo $marker;
	if(empty($_GET['nocluster'])) {
		echo 'var markers = L.markerClusterGroup();';
		echo $clusters;
		echo 'carte.addLayer(markers);';
	}
   echo ' </script>';
}
?>
</body>
</html>
